# Welcome!
If you are on this page, it might mean you were trying to go to [MineWeb](/mineweb). Otherwise, you got here because of an error, because this site, unlike the MineWeb site, was *just* started.
